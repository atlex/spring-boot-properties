package org.am;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * The TestClass class.
 *
 * @author Alexander Maximenya
 * @version 2017-03-17
 */
@Component
public class TestClass {

    @Autowired
    private ApplicationConfiguration config;

    @PostConstruct
    public void printProperties() {
        System.out.println();
        System.out.println("mongodbUrl = " + config.getMongodbUrl());
        System.out.println();
    }
}
