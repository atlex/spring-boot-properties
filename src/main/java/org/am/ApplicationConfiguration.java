package org.am;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * The ApplicationConfiguration class.
 *
 * @author Alexander Maximenya
 * @version 2017-03-17
 */
@Configuration
@ConfigurationProperties(prefix = "app")
public class ApplicationConfiguration {

    private String mongodbUrl;

    public String getMongodbUrl() {
        return mongodbUrl;
    }

    public void setMongodbUrl(String mongodbUrl) {
        this.mongodbUrl = mongodbUrl;
    }

}
