# Spring Boot external properties

### Build

    gradle clean build
    
### Run

    java -jar ./build/libs/spring-boot-properties-exec.jar

The application takes properties from

`config/application.properties`

You may specify different properties location

    java -jar ./build/libs/spring-boot-properties-exec.jar --spring.config.location=/etc/application.properties

E.g.

    /dir1/
        /app/spring-boot-properties-exec.jar
        /etc/application.properties

    java -jar /dir1/app/spring-boot-properties-exec.jar --spring.config.location=/dir1/etc/application.properties
    
---
